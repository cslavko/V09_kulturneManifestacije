package model;

//model klase:
public class Grad {

	//atributi:
	protected int ptt; //primarni kljuc u tabeli GRAD
	protected String naziv; //naziv grada

	//konstruktor bez parametara:
	public Grad() {
	}

	//konstruktor sa parametrima:
	public Grad(int ptt, String naziv) {
		this.ptt = ptt;
		this.naziv = naziv;
	}

	//prebacivanje objekta Grad u string (tekstualnu) reprezentaciju:
	@Override
	public String toString() {
		return "Grad [ptt=" + ptt + "; naziv=" + naziv + "]";
	}
	
	//get i set metode:
	public int getPtt() {
		return ptt;
	}
	
	public void setPtt(int ptt) {
		this.ptt = ptt;
	}
	
	public String getNaziv() {
		return naziv;
	}
	
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
